--Creating database and its tables

CREATE DATABASE BOOKS


USE BOOKS

CREATE TABLE Persona(
	persona_id SMALLINT PRIMARY KEY NOT NULL,
	nombre VARCHAR(20) NOT NULL,
	apellido VARCHAR(20) NULL,
	genero CHAR(1) NOT NULL,
	fechaNacimiento DATE NULL,
	pais VARCHAR(20) NULL
);

CREATE TABLE Libro(
	libro_id SMALLINT NOT NULL,
	titulo VARCHAR(50) NOT NULL,
	precio DECIMAL(5,2) NULL,
	persona_id SMALLINT NOT NULL,
	PRIMARY KEY (libro_id),
	FOREIGN KEY(persona_id) REFERENCES Persona (persona_id)
);


-- 1) Registrar 7 personas con datos completos
INSERT INTO Persona 
VALUES		(1, 'Ana', 'Almeida', 'F', '1990-02-14', 'Brasil'),
			(2, 'Bill', 'Bush',	'M', '1970-02-14', 'EEUU'),
			(3, 'Jorge', 'Gonzales', 'M', '1995-10-21', 'Bolivia'),	
			(4, 'Juan', 'Garcia',	'M', '1967-03-22', 'Colombia'),
			(5, 'Josh', 'White',	'M', '1976-11-10', 'EEUU'),
			(6, 'Sandra', 'Gutierrez', 'F', '1992-10-28', 'Bolivia'),
			(7, 'Gabriela', 'Suarez', 'F', '1994-01-10', 'Bolivia');

-- 2) Registrar 3 personas sin nacionalidad
INSERT INTO Persona 
VALUES		(8, 'Ana', 'Aveiro', 'F', '1994-05-01', NULL),
			(9, 'Santos', 'Oliveira',	'M', '1992-12-03', NULL),
			(10, 'Santiago', 'Quiroga', 'M', '2000-11-23', NULL);	

--3) Registrar una persona sin nacionalidad ni fecha de nacimiento
INSERT INTO Persona 
VALUES		(11, 'Olivia', 'Vargas', 'F', NULL, NULL);

--4) Registrar 10 libros
INSERT INTO Libro 
VALUES		(01, 'Las Cenizas de Angela', '300.45', '6'),
			(02, 'Edipo Rey', '123.66', '6'),
			(03, 'La vengadora de las mujeres', '122.87', '7'),
			(04, 'Azul', '45.77', '9'),
			(05, 'Titanic', '30.45', '3'),
			(06, 'Amor perdido', '12.11', '4'),
			(07, 'La Superacion Personal', '456.78', '2'),
			(08, 'Los Tuneles', '23.89', '2'),
			(09, 'Barco', '15.89', '7'),
			(010, 'La Estancia', '12.89', '8');

--5) Listar el nombre y apellido de la tabla Persona ordanadas por apellido y luego por nombre

SELECT apellido, nombre
FROM Persona
ORDER BY apellido, nombre

--6) Listas los libros con precio mayor a 50 bs y que el titulo del libro comience con la letra "A"

SELECT titulo
FROM Libro
WHERE precio>50 AND LEFT(titulo,1)='A'  

--7) Listar el nombre y apellido de las personas que tengan al menos un libro cuyo precio sea mayor a 50 Bs.

SELECT DISTINCT nombre, apellido
FROM Persona
WHERE persona_id 
IN(SELECT persona_id FROM Libro WHERE precio>50) 


--8) Listar el nombre y apellido de las personas que nacieron el a�o 1990

SELECT nombre, apellido
FROM Persona
WHERE fechaNacimiento BETWEEN '1990-01-01' AND '1990-12-31'


--9) Listar los libros donde el titulo del libro contenga la letra �e� en la segunda posici�n

SELECT *
FROM Libro
WHERE titulo like '_e%' 

--10) Contar el numero de registros de la tabla �Persona�

SELECT count(*)
FROM Persona

--11) Listar el o los libros m�s baratos
SELECT MIN (precio) MinPrecio
FROM Libro

--12) Listar el o los libros m�s caros      
SELECT MAX (precio) MaxPrecio
FROM Libro

--13) Mostrar el promedio de precios de los libros
SELECT sum(precio) / count(*) Promedio
FROM Libro

--14) Mostrar la sumatoria de los precios de los libros
SELECT sum(precio)
FROM Libro

--15) Modificar la longitud del campo titulo para que soporte hasta titulos de 300 caracteres
ALTER TABLE Libro
ALTER COLUMN titulo VARCHAR(300);

--16) Listar las personas que sean del pa�s de Colombia � de Mexico
SELECT * 
FROM Persona 
WHERE Persona.pais = 'Colombia' OR Persona.pais = 'Mexico';

--17) Listar los libros que no empiezen con la letra �T�
SELECT * 
FROM Libro WHERE NOT LEFT(Libro.titulo, 1)='T';

--18) Listar los libros que tengan un precio entre 50 a 70 Bs.
SELECT * 
FROM Libro 
WHERE precio BETWEEN 50 AND 70

--19) Listar las personas AGRUPADOS por pa�s
SELECT Persona.pais, COUNT(*) as Cantidad 
FROM Persona 
GROUP BY Persona.pais;

--20) Listar las personas AGRUPADOS por pa�s y genero
SELECT Persona.pais, Persona.genero, COUNT(*) as Cantidad 
FROM Persona 
GROUP BY Persona.pais, Persona.genero;

--21) Listar las personas AGRUPADOS por genero, cuya cantidad sea mayor a 5
SELECT Persona.genero, COUNT(*) as Cantidad 
FROM Persona 
GROUP BY Persona.genero
HAVING COUNT(Persona.persona_id) > 5;

--22) Listar los libros ordenados por titulo en orden descendente
SELECT * FROM Libro
ORDER BY titulo DESC;

--23) Eliminar el libro con el titulo �SQL Server�
DELETE FROM Libro
WHERE titulo='SQL Server'

--24) Eliminar los libros que comiencen con la letra �A�
DELETE FROM Libro
WHERE LEFT(titulo, 1) = 'A';

--25) Eliminar las personas que no tengan libros
DELETE FROM Persona
WHERE persona_id NOT IN (SELECT persona_id FROM Libro);

--26) Eliminar todos los libros
TRUNCATE TABLE Libro;

--27) Modificar el nombre de la persona llamada �Marco� por �Marcos�
UPDATE Persona SET nombre='Marcos'
WHERE nombre='Marco'

--28) Modificar el precio del todos los libros que empiezen con la palabra �Calculo�  a  57 Bs.
UPDATE Libro SET precio=57.00
WHERE titulo LIKE 'Calculo %'

--29) Inventarse una consulta con GROUP BY  y HAVING  y explicar el resultado que retorna.
SELECT precio, COUNT(*) as quantity
FROM Libro
WHERE precio IS NOT NULL
GROUP BY precio
HAVING COUNT(precio) > = 1