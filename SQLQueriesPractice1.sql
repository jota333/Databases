--1) Creating Database called Tienda
CREATE DATABASE Tienda

--2) Removing Database named Tienda
DROP DATABASE Tienda

--3) Creating Tables called: Client, Products and Sells
CREATE TABLE Client(
ClientID int,
FirstName varchar(255),
Apellido varchar(255),
Age int
);

CREATE TABLE Products(
ProductId int,
ProductName varchar(255),
Expiration date,
price float
);

CREATE TABLE Sells(
SellCode int,
sellDate date,
seePrice float
);

--4) Removing tables created in point 3
DROP TABLE Client, Products, Sells;

--5)Modifications to column name 'Apellido' to 'Apellidos'
EXEC sp_RENAME 'Client.Apellido', 'Apellidos', 'COLUMN'

--6) Empty (remove all the records) in the 'Sells' table, first we must insert values in the Sells table
INSERT INTO Sells
VALUES ('23434', '1992-12-12', '23.44')
--Checking the values were added correctly
SELECT * FROM Sells 
--Empty the whole table
TRUNCATE TABLE Sells;


--7) Insert 5 records in the tables: Client, Products, Sells
INSERT INTO Client
VALUES ('124', 'Pedro', 'Mendizabal', '55')
INSERT INTO Client
VALUES ('125', 'Lizandra', 'Butron', '24')
INSERT INTO Client
VALUES ('126', 'Aneliese', 'Fernandez', '25')
INSERT INTO Client
VALUES ('124', 'Jose', 'Garcia', '26')


INSERT INTO Products
VALUES ('5678', 'Milkyway', '2018-08-25', '55.65')
INSERT INTO Products
VALUES ('5411', 'Cheese', '2018-08-28', '75.99')
INSERT INTO Products
VALUES ('1100', 'Corona Beer', '2018-12-25', '15.65')
INSERT INTO Products
VALUES ('9832', 'Chocolate Chips', '2019-08-25', '120.65')
INSERT INTO Products
VALUES ('5678', 'Baked pork', '2018-08-27', '455.65')


INSERT INTO Sells
VALUES ('3422', '2018-03-10', '8.23')
INSERT INTO Sells
VALUES ('6452', '2018-02-01', '238.77')
INSERT INTO Sells
VALUES ('1111', '2018-01-16', '65.32')
INSERT INTO Sells
VALUES ('3460', '2018-05-19', '11.43')
INSERT INTO Sells
VALUES ('1200', '2018-06-06', '3.23')

--8) List all the sells
SELECT * FROM Sells

--9) List top 10 Clients
SELECT TOP 10 * FROM Client

--10) List all clients grouped by age (List the number of clients whose ages are equal to 25)
SELECT COUNT (ClientID), Age
FROM Client
GROUP BY Age
HAVING Age= 25


--11) List clientes whose ages are greater than 20
SELECT * FROM Client
Where Age > 20

--12) Remove all clients whose ages are equal or greater than 30
DELETE FROM Client 
WHERE Age >= 30


USE Tienda
SELECT * FROM Client